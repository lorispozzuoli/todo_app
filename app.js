const taskInput = document.querySelector("#input");

const inputButton = document.querySelector("#input-button");

const taskList = document.querySelector("#task-list");

//ascoltiamo quando l'utente fa click ed accade qualcosa e facciamo fare la funzione addTask
inputButton.addEventListener("click", addTask);
//Per cancellare con la X dobbiamo ascoltare con un altro evento creando una funzione
taskList.addEventListener("click", removeTask);

//quando clicchiamo il bottone inviamo un parametro e ci da degli eventi con varie informazioni
function addTask(e) {
  // console.log(e)
  //con taskinput.value posso vedere il valore che scriviamo e se è uguale ad una stringa vuota allora fa un alert
  if (taskInput.value === "") {
    alert("Scrivi qualcosa")
    //nel caso non si scrive nulla non esce la x
    link.innerHTML = "";
  }
  //permette di creare degli elementi, creare nuovo li e aggiunge classe
  const li = document.createElement("li");
  li.className = "task";
  //serve per inserire quello che scriviamo nel primo input e con create text node sarà un testo, creare nuovo text node e metterlo dentro li di js
  li.appendChild(document.createTextNode(taskInput.value));

  //creare link
  const link = document.createElement("a");

  //aggiungere classe ad <a>
  link.className = "delete-todo";

  //aggiungi x al <a> in html oppure un button
  link.innerHTML = "<button class='bottone-cancella'>X</button>";

  //mettere link nel li
  li.appendChild(link);

  //mettere li dentro ul
  taskList.appendChild(li);

  //per pulire quando scriviamo
  taskInput.value = "";



  //per eventuali problemi al click, che se l'evento non viene gestito in modo esplicito, l'azione predefinita non deve essere intrapresa come normalmente.
  e.preventDefault();
}

function removeTask(e) {
  if (e.target.parentElement.classList.contains("delete-todo")) {
    //   console.log('cancella-todo')
    //serve per risalire al nostro li originale mettendo due parent element
    e.target.parentElement.parentElement.remove();
  }
}
